import Link from "next/link";
import styles from "./footer.module.css";

export default function Footer() {
  return (
    <footer className={styles.container}>
      <img
        src="/images/footer-logos.png"
        alt="logos do rodapé"
        className={styles.logos}
      />

      <div className={styles.footerText}>
        <div>
          R. Vinte e Cinco de Março, 268 - Centro, Fortaleza - CE, 60060-120
          <br />
          observatoriodefortaleza@iplanfor.fortaleza.ce.gov.br
        </div>
      </div>
    </footer>
  );
}
