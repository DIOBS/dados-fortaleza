import Link from "next/link";
import styles from "./header.module.css";

export default function Header() {
  return (
    <div className={styles.container}>
            <img
            src="/images/dadosfor.png"
            alt="logo dados fortaleza"
            className={styles.logo}
            />

      <div className={styles.navbar}>
        <Link href="/">Início</Link>
        <Link href="/">Visualizações</Link>
        <Link href="/">Cultura de Dados</Link>
        <Link href="/">Datalake</Link>
      </div>
    </div>
  );
}
