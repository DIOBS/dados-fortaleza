import { Panel } from "@/app/lib/definitions";
import styles from "./panelItem.module.css";

type Props = {
  panel: Panel;
};

export default function PanelItem({ panel }: Props) {
    const myImageStyle = { width: `${panel.imgWidth}vw` };
  return (
    <div className={styles.container}>
      <div className={styles.containerImg}>
        <img
          src={`/images/${panel.coverImg}`}
          alt="capa do painel"
          style={myImageStyle}
        />
      </div>

      <div className={styles.content}>
        <div>{panel.title}</div>
        <button className={styles.accessButton}>Acesse</button>
      </div>
    </div>
  );
}
