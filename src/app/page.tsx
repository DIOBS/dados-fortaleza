import PanelItem from "@/components/PanelItem/PanelItem";
import panels from "./lib/placeholder-data";
import styles from "./page.module.css";

export default function Home() {
  return (
    <main className={styles.main}>
      <div className={styles.topPage}>
        <h1>PAINÉIS DO OBSERVATÓRIO</h1>
        <div className={styles.topContent}>
          {panels.map((panel, index) => (
            <PanelItem panel={panel} key={index} />
          ))}
        </div>
      </div>
    </main>
  );
}
