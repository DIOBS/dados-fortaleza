export type Panel = {
    id: number;
    title: string;
    coverImg: string;
    imgWidth: number;
    urlPage: string;
}