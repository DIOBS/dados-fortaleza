import { Panel } from "./definitions";

const panels: Panel[] = [
  {
    id: 1,
    title: "Taxa de natalidade, fecundidade, mortalidade",
    coverImg: "comparativo_bairros.svg",
    imgWidth: 6.63,
    urlPage:
      "https://observatoriodefortaleza.fortaleza.ce.gov.br/dados/painel/comparativo_bairros/",
  },
  {
    id: 2,
    title: "Escolas de Educação Básica no Ceará",
    coverImg: "escolas_ceara.svg",
    imgWidth: 4.2,
    urlPage:
      "https://observatoriodefortaleza.fortaleza.ce.gov.br/dados/painel/escolas_educacao_basica/",
  },
  {
    id: 3,
    title: "Atlas da Economia de Fortaleza",
    coverImg: "atlas_economia.svg",
    imgWidth: 5.92,
    urlPage:
      "https://observatoriodefortaleza.fortaleza.ce.gov.br/dados/painel/atlas_economia/",
  },
  {
    id: 4,
    title: "Saneamento básico no município de Fortaleza",
    coverImg: "saneamento.svg",
    imgWidth: 5.98,
    urlPage:
      "https://observatoriodefortaleza.fortaleza.ce.gov.br/dados/painel/saneamento/",
  },
  {
    id: 5,
    title:
      "Indicadores Socioeconômicos e Estudo da Evolução Urbana de Bairros de Fortaleza",
    coverImg: "bairrosfortaleza.svg",
    imgWidth: 6.2,
    urlPage: "https://iplanfor.gitbook.io/segov/",
  },
  {
    id: 6,
    title:
      "Diagnóstico de Seis Áreas de Fortaleza Identificadas como de Alta Vulnerabilidade",
    coverImg: "atuacao.svg",
    imgWidth: 3.63,
    urlPage: "https://iplanfor.gitbook.io/previo/",
  },
];

export default panels;
